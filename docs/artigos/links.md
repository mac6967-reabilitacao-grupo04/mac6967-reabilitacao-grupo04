Links dos artigos base

# Artigo principal

## Global Need for Physical Rehabilitation: Systematic Analysis from the Global Burden of Disease Study 2017

[a link] https://www.researchgate.net/publication/331864550_Global_Need_for_Physical_Rehabilitation_Systematic_Analysis_from_the_Global_Burden_of_Disease_Study_2017


# Artigos secundários

## Physical Rehabilitation Needs in the BRICS Nations from 1990 to 2017: Cross-National Analyses Using Data from the Global Burden of Disease Study 

[a link] https://www.ncbi.nlm.nih.gov/pmc/articles/PMC7312462/

## Physical Rehabilitation Needs Per Condition Type: Results From the Global Burden of Disease Study 2017

[a link] https://pubmed.ncbi.nlm.nih.gov/32035140/

## The need to scale up rehabilitation

[a link] https://apps.who.int/iris/handle/10665/331210

# Artigos GBD

## Global, regional, and national incidence, prevalence, and years lived with disability for 354 diseases and injuries for 195 countries and territories, 1990–2017: a systematic analysis for the Global Burden of Disease Study 2017

[a link] https://www.thelancet.com/journals/lancet/article/PIIS0140-6736(18)32279-7/fulltext


## Measuring global health: motivation and evolution of the Global Burden of Disease Study

[a link] https://pubmed.ncbi.nlm.nih.gov/28919120/
