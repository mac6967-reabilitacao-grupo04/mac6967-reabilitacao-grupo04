# MAC6967-REABILITAÇÃO - GRUPO 04

 Predição de necessidade de reabilitação de pacientes IRLM+IMREA-HC-FMUSP

Criar um modelo e avaliar a necessidade de reabilitação de indivíduos de acordo com dados sobre condições de saúde ao longo do tempo, estratificando os resultados por grupos de incapacidade (mobilidade, visão, audição, cognição, comunicação), nível de incapacidade, impacto da incapacidade (por anos vividos com incapacidade) e estados/regiões brasileiras.


# 1. Questões de Pesquisa 

## 1.1. Pergunta de pesquisa principal

Qual é a necessidade de reabilitação (de diferentes naturezas e intensidades) por
condições de saúde ao longo do tempo, com os resultados estratificados por grupos de
incapacidade (vários domínios funcionais: físico, visão, audição, cognição), nível de
incapacidade (por faixas de severidade [disability weight]), impacto da incapacidade (por
anos vividos com incapacidade) e estados/regiões administrativas brasileiras?

## 1.2. Perguntas de pesquisa secundárias

Qual é o tamanho da estimativa a nível nacional e estadual de anos vividos com
deficiência (YLD, do inglês: Years lived with disability) – para cada grupo de
incapacidade – utilizando-se a as métricas disponíveis na base dados tais como contagem
de YLDs; taxas de YLDs / 100.000 habitantes; e taxas YLDs padronizadas por idade?
Qual a porcentagem a nível nacional e estadual de YLDs com probabilidade de se
beneficiar de reabilitação por grupo de incapacidade, relativo ao total de YLDs?

Alguma dessas estimativas mudou significativamente durante o tempo? Se sim, o
quanto mudou?

Por último, alguma dessas tendências diferiu entre estados com diferentes níveis
de renda?

Quais estados brasileiros têm mais necessidade de reabilitação por grupo de
incapacidade?

Quais sexos têm mais necessidade de reabilitação por grupo de incapacidade?

Quais idades têm mais necessidade de reabilitação por grupo de incapacidade?

Como tem sido essas tendências ao longo do tempo?
